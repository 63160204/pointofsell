/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.patthamawan.orderproject;

/**
 *
 * @author Oil
 */
public interface OnBuyProductLister {
    public void buy(Product product, int amount);
    
}
